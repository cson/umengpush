package umengpush

import (
	"testing"
)

func TestIosNotification(t *testing.T) {
	n := NewIosNotification(appkey)
	n.SetTestMode() //开启测试模式
	n.SetUnitCastMode("AqC_YbCwMqEjasc-DdFdNAkqltqHQHXsY2eeOW2kGkMI")
	n.SetNormalPushMode("imtitle", "imsubtitle", "imbody")

	//client := NewUPushClient(appkey, app_master_secret)
	//response, err := client.Push(n)
	//if err != nil {
	//	t.Error(err)
	//	return
	//}
	//fmt.Println(response.String())
	//if response.Ret != "SUCCESS" {
	//	t.Error("request error")
	//	return
	//}
}

//测试友盟推送文档给出的参数限制
//测试ios推送；android与ios推送传参不一样
func TestIosNotificationCheckParam(t *testing.T) {
	n := NewIosNotification("")
	err := n.CheckParamValid()
	if err == nil {
		t.Error("app key is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("app master secret is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("case type is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetUnitCastMode("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("unicast device token is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetListCastMode(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("listcast device token is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetGroupCastMode(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("groupcast filter is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetFileCastMode("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("filecast file id is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetCustomizedFileMode("fileid", "")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias type is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetCustomizedFileMode("", "alias type")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast file id is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetCustomizedAliasMode(nil, "alias type")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetCustomizedAliasMode([]string{"alias"}, "")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias type is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetNormalPushMode("", "title", "text")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification title is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetNormalPushMode("ticker", "", "text")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification subtitle is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetNormalPushMode("ticker", "title", "")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification body is null but no error")
		return
	}

	n = NewIosNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetNormalPushMode("ticker", "title", "body")
	n.SetBadge("x")
	n.SetSound("x")
	n.SetCategory("x")
	n.SetKey("d", "d")
	n.SetKey("p", "p")

	if len(n.getPayload()) != 1 {
		t.Error("ios notification aps set 'd' or 'p' can not be successful")
		return
	}

}
