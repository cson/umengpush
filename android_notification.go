package umengpush

import "errors"

type androidNotification struct {
	*basicNotification
}

func NewAndroidNotification(appkey string) *androidNotification {
	n := newBasicNotification(appkey)
	n.data.Payload = &androidNotificationPayload{
		Body: &androidNotificationBody{
			PlayVibrate: "true",
			PlayLights:  "true",
			PlaySound:   "true",
			AfterOpen:   go_app,
		},
	}
	return &androidNotification{n}
}

type androidNotificationBody struct {
	// 当DisplayType=message时，Body的内容只需填写custom字段。
	// 当DisplayType=notification时，Body包含如下参数:
	// 通知展现内容:
	Ticker string `json:"ticker"` // 必填，通知栏提示文字
	Title  string `json:"title"`  // 必填，通知标题
	Text   string `json:"text"`   // 必填，通知文字描述

	// 自定义通知图标:
	Icon string `json:"icon,omitempty"` // 可选，状态栏图标ID，R.drawable.[smallIcon]，
	// 如果没有，默认使用应用图标。
	// 图片要求为24*24dp的图标，或24*24px放在drawable-mdpi下。
	// 注意四周各留1个dp的空白像素
	LargeIcon string `json:"largeIcon,omitempty"` // 可选，通知栏拉开后左侧图标ID，R.drawable.[largeIcon]，
	// 图片要求为64*64dp的图标，
	// 可设计一张64*64px放在drawable-mdpi下，
	// 注意图片四周留空，不至于显示太拥挤
	Img string `json:"img,omitempty"` // 可选，通知栏大图标的URL链接。该字段的优先级大于largeIcon。
	// 该字段要求以http或者https开头。

	// 自定义通知声音:
	Sound string `json:"sound,omitempty"` // 可选，通知声音，R.raw.[sound]。
	// 如果该字段为空，采用SDK默认的声音，即res/raw/下的
	// umeng_push_notification_default_sound声音文件。如果
	// SDK默认声音文件不存在，则使用系统默认Notification提示音。

	// 自定义通知样式:
	BuilderId int `json:"builder_id,omitempty"` // 可选，默认为0，用于标识该通知采用的样式。使用该参数时，
	// 开发者必须在SDK里面实现自定义通知栏样式。

	// 通知到达设备后的提醒方式，注意，"true/false"为字符串
	PlayVibrate string `json:"play_vibrate,omitempty"` // 可选，收到通知是否震动，默认为"true"
	PlayLights  string `json:"play_lights,omitempty"`  // 可选，收到通知是否闪灯，默认为"true"
	PlaySound   string `json:"play_sound,omitempty"`   // 可选，收到通知是否发出声音，默认为"true"

	// 点击"通知"的后续行为，默认为打开app。
	// 可选，默认为"go_app"，值可以为:
	//   "go_app": 打开应用
	//   "go_url": 跳转到URL
	//   "go_activity": 打开特定的activity
	//   "go_custom": 用户自定义内容。
	AfterOpen AfterOpenAction `json:"after_open,omitempty"`

	// 当after_open=go_url时，必填。
	// 通知栏点击后跳转的URL，要求以http或者https开头
	Url string `json:"url,omitempty"`

	// 当after_open=go_activity时，必填。
	// 通知栏点击后打开的Activity
	Activity string `json:"activity,omitempty"`

	// 当DisplayType=message时, 必填
	// 当DisplayType=notification且after_open=go_custom时，必填
	// 用户自定义内容，可以为字符串或者JSON格式。
	Custom interface{} `json:"custom,omitempty"`
}

type androidNotificationPayload struct {
	DisplayType DisplayType              `json:"display_type"` // 必填，消息类型: notification(通知)、message(消息)
	Body        *androidNotificationBody `json:"body"`         // 必填，消息体

	// 可选，JSON格式，用户自定义key-value。只对"通知"(DisplayType=notification)生效。
	// 可以配合通知到达后，打开App/URL/Activity使用。
	//"key1": "value1",
	Extra map[string]string `json:"extra,omitempty"`
}

func (a *androidNotificationPayload) checkPayloadParam() error {
	if a.DisplayType != message && a.DisplayType != notification {
		return errors.New("android notification payload display type is not 'message' or 'notification'")
	}
	if a.DisplayType == message {
		if a.Body.Custom == nil {
			return errors.New("android notification payload display type is 'message', custom need to be string or json object")
		}
	} else {
		if a.Body.Title == "" || a.Body.Ticker == "" || a.Body.Text == "" {
			return errors.New("android notification payload display type is 'notification', 'title' 'ticker' 'text' can not be null")
		}
		if a.Body.Custom == nil && a.Body.AfterOpen == go_custom {
			return errors.New("android notification payload display type is 'message', custom need to be string or json object")
		}
	}
	if a.Body.AfterOpen == go_url && a.Body.Url == "" {
		return errors.New("android notification payload.Body after_open is 'go_url', url can not be null")
	}
	if a.Body.AfterOpen == go_activity && a.Body.Activity == "" {
		return errors.New("android notification payload.Body after_open is 'go_activity', activity can not be null")
	}
	return nil
}

type DisplayType string

const (
	message      DisplayType = "message"      //消息送达到用户设备后，消息内容透传给应用自身进行解析处理
	notification DisplayType = "notification" //消息送达到用户设备后，由友盟SDK接管处理并在通知栏上显示通知内容
)

type AfterOpenAction string

const (
	go_app      AfterOpenAction = "go_app"      //"go_app": 打开应用
	go_url      AfterOpenAction = "go_url"      //"go_url": 跳转到URL
	go_activity AfterOpenAction = "go_activity" //"go_activity": 打开特定的activity
	go_custom   AfterOpenAction = "go_custom"   //"go_custom": 用户自定义内容。
)

// 可选，默认为false。当为true时，表示MIUI、EMUI、Flyme系统设备离线转为系统下发
func (n *androidNotification) getPayload() *androidNotificationPayload {
	return n.data.Payload.(*androidNotificationPayload)
}

// 可选，默认为false。当为true时，表示MIUI、EMUI、Flyme系统设备离线转为系统下发
func (n *androidNotification) SetMiPush(mipush bool) {
	if mipush {
		n.data.MiPush = "true"
	} else {
		n.data.MiPush = "false"
	}
}

// 可选，mipush值为true时生效，表示走系统通道时打开指定页面acitivity的完整包路径。
func (n *androidNotification) SetMiActivity(miActivity string) {
	n.data.MiActivity = miActivity
}

///发送限速，每秒发送的最大条数。
func (n *androidNotification) SetMaxSendNum(num int) {
	n.data.Policy.MaxSendNum = num
}

//////

//ticker 通知栏提示文字
//title 通知标题
//text 通知文字描述
//custom 用户自定义内容，可以为字符串或者JSON格式
func (n *androidNotification) SetDisplayNotification(ticker, title, text string, custom interface{}) {
	n.getPayload().DisplayType = notification
	n.getPayload().Body.Ticker = ticker
	n.getPayload().Body.Title = title
	n.getPayload().Body.Text = text
	n.getPayload().Body.Custom = custom
}

//custom 用户自定义内容，可以为字符串或者JSON格式
func (n *androidNotification) SetDisplayMessage(custom interface{}) {
	n.getPayload().DisplayType = message
	n.getPayload().Body.Custom = custom
}

///用于标识该通知采用的样式。使用该参数时, 必须在SDK里面实现自定义通知栏样式。
func (n *androidNotification) SetBuilderId(builderId int) {
	n.getPayload().Body.BuilderId = builderId
}

///状态栏图标ID, R.drawable.[smallIcon],如果没有, 默认使用应用图标。
func (n *androidNotification) SetIcon(icon string) {
	n.getPayload().Body.Icon = icon
}

///通知栏拉开后左侧图标ID
func (n *androidNotification) SetLargeIcon(largeIcon string) {
	n.getPayload().Body.LargeIcon = largeIcon
}

///通知栏大图标的URL链接。该字段的优先级大于largeIcon。该字段要求以http或者https开头。
func (n *androidNotification) SetImg(img string) {
	n.getPayload().Body.Img = img
}

///收到通知是否震动,默认为"true"
func (n *androidNotification) SetPlayVibrate(playVibrate bool) {
	if playVibrate {
		n.getPayload().Body.PlayVibrate = "true"
	} else {
		n.getPayload().Body.PlayVibrate = "false"
	}
}

///收到通知是否闪灯,默认为"true"
func (n *androidNotification) SetPlayLights(playLights bool) {
	if playLights {
		n.getPayload().Body.PlayLights = "true"
	} else {
		n.getPayload().Body.PlayLights = "false"
	}
}

///收到通知是否发出声音,默认为"true"
func (n *androidNotification) SetPlaySound(playSound bool) {
	if playSound {
		n.getPayload().Body.PlaySound = "true"
	} else {
		n.getPayload().Body.PlaySound = "false"
	}
}

///通知声音，R.raw.[sound]. 如果该字段为空，采用SDK默认的声音
func (n *androidNotification) SetSound(sound string) {
	n.getPayload().Body.Sound = sound
}

///点击"通知"的后续行为，默认为打开app。
func (n *androidNotification) SetGoAppAfterOpen() {
	n.getPayload().Body.AfterOpen = go_app
}

///点击"通知"的后续行为，默认为打开特定的activity。
func (n *androidNotification) SetGoActivityAfterOpen(activity string) {
	n.getPayload().Body.AfterOpen = go_activity
	n.getPayload().Body.Activity = activity
}

///点击"通知"的后续行为，默认为打开跳转到URL。
func (n *androidNotification) SetGoUrlAfterOpen(url string) {
	n.getPayload().Body.AfterOpen = go_url
	n.getPayload().Body.Url = url
}

///点击"通知"的后续行为，用户自定义内容，可以为字符串或者JSON格式
func (n *androidNotification) SetGoCustomAfterOpen(custom interface{}) {
	n.getPayload().Body.AfterOpen = go_custom
	n.getPayload().Body.Custom = custom
}
