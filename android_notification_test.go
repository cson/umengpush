package umengpush

import (
	"fmt"
	"testing"
	"time"
)

var appkey string
var app_master_secret string

func init() {
	appkey = "5b236cd2f29d98459e000038"
	app_master_secret = "v9kk9n8ckarwyd9krtsqgabhljmxcjop"
}

func TestAndroidCustomizedWithAlias(t *testing.T) {
	//自定义播，使用alias
	client := NewUPushClient(appkey, app_master_secret)
	{
		//自定义播，使用alias,推送通知
		n := NewAndroidNotification(appkey)
		n.SetDisplayNotification("CustomizedWithAlias ticker", "CustomizedWithAlias title", "CustomizedWithAlias text", "TestAndroidCustomizedWithAlias")
		n.SetTestMode() //开启测试模式
		n.SetCustomizedAliasMode([]string{"UserId-1", "UserId-10"}, "UserId")

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}

	time.Sleep(time.Second)

	{
		//自定义播，使用alias推送消息
		n := NewAndroidNotification(appkey)
		n.SetDisplayMessage("TestAndroidCustomizedWithAlias, receive?")
		n.SetTestMode() //开启测试模式
		n.SetCustomizedAliasMode([]string{"UserId-1", "UserId-10"}, "UserId")

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}
}

func TestAndroidCustomizedWithFile(t *testing.T) {
	//自定义播，使用文件
	//alias type 与 alias 是客户端调用pushAgent.addAlias()添加的
	client := NewUPushClient(appkey, app_master_secret)
	response, err := client.UploadFile([]string{"UserId-1", "UserId-10"})
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(response.String())
	if response.Ret != "SUCCESS" {
		t.Error("request error")
		return
	}
	var fileId = response.Data.FileId

	{
		//自定义文件播推送通知
		n := NewAndroidNotification(appkey)
		n.SetDisplayNotification("im CustomizedWithFile ticker", "im CustomizedWithFile title", "im CustomizedWithFile text", "test CustomizedWithFile")
		n.SetTestMode() //开启测试模式
		n.SetCustomizedFileMode(fileId, "UserId")

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}

	time.Sleep(time.Second)

	{
		//自定义文件播推送消息
		n := NewAndroidNotification(appkey)
		n.SetDisplayMessage("CustomizedWithFile, receive?")
		n.SetTestMode() //开启测试模式
		n.SetCustomizedFileMode(fileId, "UserId")

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}
}

func TestAndroidFileCast(t *testing.T) {
	client := NewUPushClient(appkey, app_master_secret)
	response, err := client.UploadFile([]string{"Aljt2WB0TH0clFy2FU9-zcJ-5j1D7vM4SYN7ugp3yO7C", "Aia9omJhvzhnEspY1UEoSew87GO3yx2X-DBdJq70N_xq"})
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(response.String())
	if response.Ret != "SUCCESS" {
		t.Error("request error")
		return
	}

	//PF060551540260879666
	var fileId = response.Data.FileId

	{
		//文件播推送通知
		n := NewAndroidNotification(appkey)
		n.SetDisplayNotification("im file ticker", "im file title", "im file text", "test file")
		n.SetTestMode() //开启测试模式
		n.SetFileCastMode(fileId)

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}

	time.Sleep(time.Second)

	{
		//文件播推送消息
		n := NewAndroidNotification(appkey)
		n.SetDisplayMessage("fileeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")
		n.SetTestMode() //开启测试模式
		n.SetFileCastMode(fileId)

		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}
}

func TestAndroidUnitCast(t *testing.T) {
	{
		//单播推送通知
		n := NewAndroidNotification(appkey)
		n.SetDisplayNotification("im unicast ticker", "im unicast title", "im unicast text", "unicast list")
		n.SetTestMode() //开启测试模式
		n.SetUnitCastMode("Aljt2WB0TH0clFy2FU9-zcJ-5j1D7vM4SYN7ugp3yO7C")

		client := NewUPushClient(appkey, app_master_secret)
		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}

	time.Sleep(time.Second)

	{
		//单播推送信息
		n := NewAndroidNotification(appkey)
		n.SetDisplayMessage(map[string]interface{}{"text": " unicast text", "key": " unicast"})
		n.SetTestMode() //开启测试模式
		n.SetUnitCastMode("Aljt2WB0TH0clFy2FU9-zcJ-5j1D7vM4SYN7ugp3yO7C")

		client := NewUPushClient(appkey, app_master_secret)
		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}
}

func TestAndroidListCast(t *testing.T) {
	{
		//多播推送通知
		n := NewAndroidNotification(appkey)
		n.SetDisplayNotification("im list ticker", "im list title", "im list text", " list test")
		n.SetTestMode() //开启测试模式
		n.SetListCastMode([]string{"Aljt2WB0TH0clFy2FU9-zcJ-5j1D7vM4SYN7ugp3yO7C", "Aia9omJhvzhnEspY1UEoSew87GO3yx2X-DBdJq70N_xq"})

		client := NewUPushClient(appkey, app_master_secret)
		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}

	time.Sleep(time.Second)

	{
		//多播推送信息
		n := NewAndroidNotification(appkey)
		n.SetDisplayMessage(map[string]interface{}{"text": "message  list", "key": " list"})
		n.SetTestMode() //开启测试模式
		n.SetListCastMode([]string{"Aljt2WB0TH0clFy2FU9-zcJ-5j1D7vM4SYN7ugp3yO7C", "Aia9omJhvzhnEspY1UEoSew87GO3yx2X-DBdJq70N_xq"})

		client := NewUPushClient(appkey, app_master_secret)
		response, err := client.Push(n)
		if err != nil {
			t.Error(err)
			return
		}
		fmt.Println(response.String())
		if response.Ret != "SUCCESS" {
			t.Error("request error")
			return
		}
	}
}

func TestAndroidBoardCast(t *testing.T) {
	//广播推送通知
	n := NewAndroidNotification(appkey)
	n.SetDisplayNotification("BoardCast ticker", "BoardCast title", "BoardCast text", "BoardCast custom")
	n.SetTestMode() //开启测试模式
	n.SetBoardCastMode()
	n.SetDescription("测试广播通知-Android")

	client := NewUPushClient(appkey, app_master_secret)
	response, err := client.Push(n)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(response.String())
	if response.Ret != "SUCCESS" {
		t.Error("request error")
		return
	}
}

func TestAndroidGroupCast(t *testing.T) {
	//组播推送通知
	n := NewAndroidNotification(appkey)
	n.SetDisplayNotification("GroupCast ticker", "GroupCast title", "GroupCast text", "GroupCast custom")
	n.SetTestMode() //开启测试模式
	n.SetGroupCastMode(map[string]interface{}{
		"where": map[string]interface{}{
			"and": []map[string]interface{}{
				{"app_version": "1.0"},
				{"launch_from": "2018-10-17"},
			},
		},
	})
	n.SetDescription("组播通知-Android")

	client := NewUPushClient(appkey, app_master_secret)
	response, err := client.Push(n)
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(response.String())
	if response.Ret != "SUCCESS" {
		t.Error("request error")
		return
	}
}

//测试友盟推送文档给出的参数限制
//测试安卓推送；android与ios推送传参不一样
func TestAndroidNotificationCheckParam(t *testing.T) {
	n := NewAndroidNotification("")
	err := n.CheckParamValid()
	if err == nil {
		t.Error("app key is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("app master secret is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("case type is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("unicast device token is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetListCastMode(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("listcast device token is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetGroupCastMode(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("groupcast filter is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetFileCastMode("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("filecast file id is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetCustomizedFileMode("fileid", "")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias type is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetCustomizedFileMode("", "alias type")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast file id is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetCustomizedAliasMode(nil, "alias type")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetCustomizedAliasMode([]string{"alias"}, "")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("customized cast alias type is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayMessage(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display message custom is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("", "title", "text", "custom")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification ticker is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("ticker", "", "text", "custom")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification title is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("ticker", "title", "", "custom")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("display notification text is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("ticker", "title", "text", nil)
	n.SetGoActivityAfterOpen("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("go activity but activity is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("ticker", "title", "text", nil)
	n.SetGoUrlAfterOpen("")
	err = n.CheckParamValid()
	if err == nil {
		t.Error("go url but url is null but no error")
		return
	}

	n = NewAndroidNotification("appkey")
	n.SetUnitCastMode("device token")
	n.SetDisplayNotification("ticker", "title", "text", nil)
	n.SetGoCustomAfterOpen(nil)
	err = n.CheckParamValid()
	if err == nil {
		t.Error("go Custom but Custom is null but no error")
		return
	}
}
