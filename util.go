package umengpush

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"log"
)

func toJson(data interface{}) (string, error) {
	bs, err := json.Marshal(data)
	if err != nil {
		return "", err
	}
	return string(bs), nil
}

func getSign(address, body, appMasterSecret string) string {
	var str = fmt.Sprintf("POST%v%v%v", address, body, appMasterSecret)
	hash := md5.New()
	hash.Write([]byte(str))
	return fmt.Sprintf("%x", hash.Sum(nil))
}

func logD(format string, v ...interface{}) {
	log.Printf("[D]"+format+"\n", v...)
}

func logE(format string, v ...interface{}) {
	log.Printf("[E]"+format+"\n", v...)
}
