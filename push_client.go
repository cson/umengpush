package umengpush

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type UPushClient struct {
	appMasterSecret, appkey string
}

func NewUPushClient(appkey, appMasterSecret string) *UPushClient {
	return &UPushClient{
		appkey:          appkey,
		appMasterSecret: appMasterSecret,
	}
}

func (c *UPushClient) Push(notification Notification) (*UPushResponse, error) {
	err := notification.CheckParamValid()
	if err != nil {
		logE("UPushClient send failed when check param err(%v)", err)
		return nil, err
	}

	notification.SetTimestamp(strconv.FormatInt(time.Now().Unix(), 10))
	body, err := notification.GetPostBody()
	if err != nil {
		logD("UPushClient GetPostBody err(%v)", err)
		return nil, err
	}

	var sign = notification.GetSign(c.appMasterSecret)
	logD("UPushClient prepare send request, at production mode(%v) sign(%v)", notification.IsProductionMode(), sign)
	response, err := c.sendRequest(body, notification.GetApiAddress()+"?sign="+sign)
	if err != nil {
		logE("UPushClient Push err(%v)", err)
		return nil, err
	}

	return response, nil
}

func (c *UPushClient) sendRequest(data, url string) (*UPushResponse, error) {
	logD("Upush send data body(%v)", data)
	request, err := http.NewRequest("POST", url, strings.NewReader(data))
	if err != nil {
		logE("UPushClient new http request url(%v) err(%v)", url, err)
		return nil, err
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		logE("UPushClient do http request url(%v) err(%v)", url, err)
		return nil, err
	}
	bs, err := ioutil.ReadAll(response.Body)
	if err != nil {
		logE("UPushClient read response body url(%v) err(%v)", url, err)
		return nil, err
	}
	var result UPushResponse
	err = json.Unmarshal(bs, &result)
	if err != nil {
		logE("UPushClient json Unmarshal response body byte length(%v) err(%v)", len(bs), err)
		return nil, err
	}
	return &result, nil
}

//文件内容, 多个device_token/alias
func (c *UPushClient) UploadFile(content []string) (*UPushResponse, error) {
	var data = strings.Join(content, "\n")
	var body, _ = toJson(map[string]interface{}{
		"appkey":    c.appkey,
		"timestamp": strconv.FormatInt(time.Now().Unix(), 10),
		"content":   data,
	})
	var sign = getSign("https://msgapi.umeng.com/upload", body, c.appMasterSecret)
	response, err := c.sendRequest(body, "https://msgapi.umeng.com/upload?sign="+sign)
	if err != nil {
		logE("UPushClient UploadFile err(%v)", err)
		return nil, err
	}

	return response, nil
}

//{
//	"ret":"SUCCESS/FAIL",
//	"data": {
//		// 当"ret"为"SUCCESS"时，包含如下参数:
//		// 单播类消息(type为unicast、listcast、customizedcast且不带file_id)返回：
//		"msg_id":"xx"
//
//		// 任务类消息(type为broadcast、groupcast、filecast、customizedcast且file_id不为空)返回：
//		"task_id":"xx"
//
//		// 当"ret"为"FAIL"时,包含如下参数:
//		"error_code":"xx",    // 错误码，详见附录I
//		"error_msg":"xx"    // 错误信息
//	}
//}
type UPushResponse struct {
	Ret  string            `json:"ret"`
	Data UPushResponseData `json:"data"`
}

func (u *UPushResponse) String() string {
	bs, _ := json.Marshal(u)
	return string(bs)
}

type UPushResponseData struct {
	MsgId     string `json:"msg_id"`
	TaskId    string `json:"task_id"`
	FileId    string `json:"file_id"` //upload file 成功时返回
	ErrorCode string `json:"error_code"`
	ErrorMsg  string `json:"error_msg"`
}
