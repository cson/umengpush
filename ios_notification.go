package umengpush

import "errors"

type iosNotification struct {
	*basicNotification
}

func NewIosNotification(appkey string) *iosNotification {
	b := newBasicNotification(appkey)
	var payload = iosNotificationPayload{}
	b.data.Payload = payload
	payload.setKey("aps", &iosAps{
		Alert: map[string]string{},
	})
	return &iosNotification{b}
}

//"payload":   // 必填，JSON格式，具体消息内容(iOS最大为2012B){
//		"aps":      // 必填，严格按照APNs定义来填写
//		{
//			"alert":""/{ // 当content-available=1时(静默推送)，可选; 否则必填。
//				// 可为JSON类型和字符串类型
//				"title":"title",
//				"subtitle":"subtitle",
//				"body":"body"
//			}
//			"badge": xx,           // 可选
//			"sound": "xx",         // 可选
//			"content-available":1  // 可选，代表静默推送
//			"category": "xx",      // 可选，注意: ios8才支持该字段。
//		},
//		"key1":"value1",       // 可选，用户自定义内容, "d","p"为友盟保留字段，
//		// key不可以是"d","p"
//		"key2":"value2",
//		...
//}
type iosNotificationPayload map[string]interface{}

func (p iosNotificationPayload) checkPayloadParam() error {
	if p.getAps().ContentAvailable != 1 {
		if len(p.getAps().Alert["title"]) == 0 {
			return errors.New("content-available != 1(非静默推送), title can not be null, please check")
		}
		if len(p.getAps().Alert["subtitle"]) == 0 {
			return errors.New("content-available != 1(非静默推送), subtitle can not be null, please check")
		}
		if len(p.getAps().Alert["body"]) == 0 {
			return errors.New("content-available != 1(非静默推送), body can not be null, please check")
		}
	}
	return nil
}

func (p iosNotificationPayload) getAps() *iosAps {
	return p["aps"].(*iosAps)
}

//用户自定义内容, "d","p"为友盟保留字段，
//key不可以是"d","p"
func (p iosNotificationPayload) setKey(key string, value interface{}) {
	if key != "d" && key != "p" {
		p[key] = value
	}
}

type iosAps struct {
	Alert            map[string]string `json:"alert,omitempty"`             //当content-available=1时(静默推送)，可选; 否则必填。
	Badge            string            `json:"badge,omitempty"`             // 可选
	Sound            string            `json:"sound,omitempty"`             // 可选
	ContentAvailable int               `json:"content_available,omitempty"` // 可选，=1代表静默推送
	Category         string            `json:"category,omitempty"`          // 可选，注意: ios8才支持该字段。
}

//设置为静默推送
func (p *iosNotification) SetSilentPushMode(title, subtitle, body string) {
	p.getPayload().getAps().ContentAvailable = 1
	if title != "" {
		p.getPayload().getAps().Alert["title"] = title
	}
	if subtitle != "" {
		p.getPayload().getAps().Alert["subtitle"] = subtitle
	}
	if body != "" {
		p.getPayload().getAps().Alert["body"] = body
	}
}

//设置为正常推送
func (p *iosNotification) SetNormalPushMode(title, subtitle, body string) {
	p.getPayload().getAps().Alert["title"] = title
	p.getPayload().getAps().Alert["subtitle"] = subtitle
	p.getPayload().getAps().Alert["body"] = body
}

func (p *iosNotification) getPayload() iosNotificationPayload {
	return p.data.Payload.(iosNotificationPayload)
}

//用户自定义内容, "d","p"为友盟保留字段，
//key不可以是"d","p"
func (p *iosNotification) SetKey(key, value string) {
	p.getPayload().setKey(key, value)
}

//可选
func (p *iosNotification) SetBadge(badge string) {
	p.getPayload().getAps().Badge = badge
}

//可选
func (p *iosNotification) SetSound(sound string) {
	p.getPayload().getAps().Sound = sound
}

//可选，注意: ios8才支持该字段
func (p *iosNotification) SetCategory(category string) {
	p.getPayload().getAps().Category = category
}
