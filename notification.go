package umengpush

import (
	"errors"
	"strings"
	"time"
)

type Notification interface {
	//获取需求Post请求的json body
	GetPostBody() (string, error)

	//设置发送时候的时间戳
	SetTimestamp(timestamp string)

	//发送前检测参数是否合法
	CheckParamValid() error

	//获取签名
	GetSign(appMasterSecret string) string

	///是否正式模式，若为false则为测试模式
	IsProductionMode() bool

	//请求发送推送api地址
	GetApiAddress() string
}

type notificationPayload interface {
	checkPayloadParam() error
}

type CaseType string

const (
	unicast        CaseType = "unicast"        //单播
	listcast       CaseType = "listcast"       //列播，
	filecast       CaseType = "filecast"       //文件播
	broadcast      CaseType = "broadcast"      //广播
	groupcast      CaseType = "groupcast"      //组播
	customizedcast CaseType = "customizedcast" //自定义播
)

type notificationPolicy struct {
	// 可选，定时发送时，若不填写表示立即发送。
	// 定时发送时间不能小于当前时间
	// 格式: "yyyy-MM-dd HH:mm:ss"。
	// 注意，start_time只对任务类消息生效。
	StartTime string `json:"start_time,omitempty"`

	// 可选，消息过期时间，其值不可小于发送时间或者
	// start_time(如果填写了的话)，
	// 如果不填写此参数，默认为3天后过期。格式同start_time
	ExpireTime string `json:"expire_time,omitempty"`

	// 可选，发送限速，每秒发送的最大条数。最小值1000
	// 开发者发送的消息如果有请求自己服务器的资源，可以考虑此参数。
	MaxSendNum int `json:"max_send_num,omitempty"`

	// 可选，开发者对消息的唯一标识，服务器会根据这个标识避免重复发送。
	// 有些情况下（例如网络异常）开发者可能会重复调用API导致
	// 消息多次下发到客户端。如果需要处理这种情况，可以考虑此参数。
	// 注意, out_biz_no只对任务类消息生效。
	OutBizNo string `json:"out_biz_no,omitempty"`

	// 可选，多条带有相同apns_collapse_id的消息，iOS设备仅展示
	// 最新的一条，字段长度不得超过64bytes
	ApnsCollapseId string `json:"apns_collapse_id,omitempty"`
}

type basicNotification struct {
	data *basicNotificationData
}

type basicNotificationData struct {
	AppKey    string `json:"appkey"`    // 必填，应用唯一标识
	Timestamp string `json:"timestamp"` // 必填，时间戳，10位或者13位均可，时间戳有效期为10分钟

	//必填，消息发送类型,其值可以为:
	//unicast-单播
	//listcast-列播，要求不超过500个device_token
	//filecast-文件播，多个device_token可通过文件形式批量发送
	//broadcast-广播
	//groupcast-组播，按照filter筛选用户群, 请参照filter参数
	//customizedcast，通过alias进行推送，包括以下两种case:
	//  - alias: 对单个或者多个alias进行推送
	//  - file_id: 将alias存放到文件后，根据file_id来推送
	CaseType CaseType `json:"type"`

	// 当type=unicast时, 必填, 表示指定的单个设备
	// 当type=listcast时, 必填, 要求不超过500个, 以英文逗号分隔
	DeviceTokens string `json:"device_tokens"`

	// 当type=customizedcast时, 必填
	// alias的类型, alias_type可由开发者自定义, 开发者在SDK中
	// 调用setAlias(alias, alias_type)时所设置的alias_type
	AliasType string `json:"alias_type"`

	// 当type=customizedcast时, 选填(此参数和file_id二选一)
	// 开发者填写自己的alias, 要求不超过500个alias, 多个alias以英文逗号间隔
	// 在SDK中调用setAlias(alias, alias_type)时所设置的alias
	Alias string `json:"alias"`

	// 当type=filecast时，必填，file内容为多条device_token，以回车符分割
	// 当type=customizedcast时，选填(此参数和alias二选一)
	//   file内容为多条alias，以回车符分隔。注意同一个文件内的alias所对应
	//   的alias_type必须和接口参数alias_type一致。
	// 使用文件播需要先调用文件上传接口获取file_id，参照"文件上传"
	FileId string `json:"file_id"`

	Filter map[string]interface{} `json:"filter,omitempty"` // 当type=groupcast时，必填，用户筛选条件，如用户标签、渠道等，参考附录G。

	Payload notificationPayload `json:"payload"`
	Policy  notificationPolicy  `json:"policy,omitempty"` // 可选，发送策略

	// 可选，正式(true)/测试(false)模式。默认为false
	// 测试模式只会将消息发给测试设备。测试设备需要到web上添加。
	// Android: 测试设备属于正式设备的一个子集。
	ProductionMode string `json:"production_mode,omitempty"`
	Description    string `json:"description,omitempty"` // 可选，发送消息描述，建议填写。
	MiPush         string `json:"mipush,omitempty"`      // 可选，默认为false。当为true时，表示MIUI、EMUI、Flyme系统设备离线转为系统下发
	MiActivity     string `json:"mi_activity,omitempty"` // 可选，mipush值为true时生效，表示走系统通道时打开指定页面acitivity的完整包路径。
}

func newBasicNotification(appkey string) *basicNotification {
	n := &basicNotification{
		data: &basicNotificationData{
			AppKey: appkey,
		},
	}
	return n
}

func (n *basicNotification) SetTimestamp(timestamp string) {
	n.data.Timestamp = timestamp
}

func (n *basicNotification) CheckParamValid() error {
	if n.data.AppKey == "" {
		return errors.New("AppKey is null, please check")
	}
	if n.data.CaseType == "" {
		return errors.New("cast type is null, please check")
	}
	if n.data.CaseType == unicast && n.data.DeviceTokens == "" {
		return errors.New("unicast need device token, please check")
	}
	if n.data.CaseType == listcast && n.data.DeviceTokens == "" {
		return errors.New("listcast need device token, please check")
	}
	if n.data.CaseType == broadcast && n.IsProductionMode() {
		return errors.New("为了您的安全，广播不能乱搞，您真的要广播的时候把我这个判断注释掉，全体用户都会收到，发错你就GG")
	}
	if n.data.CaseType == groupcast && len(n.data.Filter) == 0 {
		return errors.New("groupcast need filter, please check")
	}
	if n.data.CaseType == filecast && n.data.FileId == "" {
		return errors.New("filecast need file id, please check")
	}
	if n.data.CaseType == customizedcast && n.data.AliasType == "" {
		return errors.New("filecast need alias type, please check")
	}
	if n.data.CaseType == customizedcast && n.data.Alias == "" && n.data.FileId == "" {
		return errors.New("filecast need alias or file id, please check")
	}
	if err := n.data.Payload.checkPayloadParam(); err != nil {
		return errors.New("payload param is invalid, please check -> " + err.Error())
	}
	return nil
}

func (n *basicNotification) GetPostBody() (string, error) {
	return toJson(n.data)
}

func (n *basicNotification) GetApiAddress() string {
	return "https://msgapi.umeng.com/api/send"
}

func (n *basicNotification) GetSign(appMasterSecret string) string {
	body, _ := toJson(n.data)
	return getSign(n.GetApiAddress(), body, appMasterSecret)
}

///是否正式模式，若为false则为测试模式
func (n *basicNotification) IsProductionMode() bool {
	return n.data.ProductionMode == "true"
}

///正式模式
func (n *basicNotification) SetProductionMode() {
	n.data.ProductionMode = "true"
}

///测试模式;只会将消息发给测试设备。测试设备需要到web上添加
func (n *basicNotification) SetTestMode() {
	n.data.ProductionMode = "false"
}

///发送消息描述，建议填写。
func (n *basicNotification) SetDescription(description string) {
	n.data.Description = description
}

///定时发送时间，若不填写表示立即发送。格式: "YYYY-MM-DD hh:mm:ss"。
func (n *basicNotification) SetStartTime(startTime int64) {
	n.data.Policy.StartTime = time.Unix(startTime, 0).Format("2006-01-02 15:04:05")
}

///消息过期时间,格式: "YYYY-MM-DD hh:mm:ss"。
func (n *basicNotification) SetExpireTime(expireTime int64) {
	n.data.Policy.ExpireTime = time.Unix(expireTime, 0).Format("2006-01-02 15:04:05")
}

///开发者对消息的唯一标识，服务器会根据这个标识避免重复发送
func (n *basicNotification) SetOutBizNo(outBizNo string) {
	n.data.Policy.OutBizNo = outBizNo
}

//
//

//选择自定义播模式，自定义播(customizedcast, 且file_id不为空)默认每小时可推送300次
//alias: 对单个或者多个alias进行推送
//alias的类型, alias_type可由开发者自定义, 开发者在SDK中调用setAlias(alias, alias_type)时所设置的alias_type
func (n *basicNotification) SetCustomizedFileMode(fileId, aliasType string) {
	n.data.CaseType = customizedcast
	n.data.AliasType = aliasType
	n.data.FileId = fileId
}

//选择自定义播模式，自定义播(customizedcast且不带file_id)消息暂无推送限制
//file内容为多条alias，以回车符分隔。注意同一个文件内的alias所对应的alias_type必须和接口参数alias_type一致。使用文件播需要先调用文件上传接口获取file_id，参照"文件上传"
//alias的类型, alias_type可由开发者自定义, 开发者在SDK中调用setAlias(alias, alias_type)时所设置的alias_type
//
//alias不能超过500个
func (n *basicNotification) SetCustomizedAliasMode(alias []string, aliasType string) {
	if len(alias) > 500 {
		alias = alias[:500]
	}
	n.data.CaseType = customizedcast
	n.data.AliasType = aliasType
	n.data.Alias = strings.Join(alias, ",")
}

//unicast-单播，单播类消息暂无推送限制
func (n *basicNotification) SetUnitCastMode(deviceToken string) {
	n.data.CaseType = unicast
	n.data.DeviceTokens = deviceToken
}

//listcast-列播，要求不超过500个device_token，消息暂无推送限制
func (n *basicNotification) SetListCastMode(deviceTokens []string) {
	if len(deviceTokens) > 500 {
		deviceTokens = deviceTokens[:500]
	}
	n.data.CaseType = listcast
	n.data.DeviceTokens = strings.Join(deviceTokens, ",")
}

//filecast-文件播，多个device_token可通过文件形式批量发送，文件播(filecast)默认每小时可推送300次
func (n *basicNotification) SetFileCastMode(fileId string) {
	n.data.CaseType = filecast
	n.data.FileId = fileId
}

//广播(broadcast)默认每天可推送10次,向安装该App的所有设备发送消息
func (n *basicNotification) SetBoardCastMode() {
	n.data.CaseType = broadcast
}

//组播(groupcast)默认每分钟可推送5次,向满足特定条件的设备集合发送消息，例如: “特定版本”、”特定地域”等
func (n *basicNotification) SetGroupCastMode(filter map[string]interface{}) {
	n.data.CaseType = groupcast
	n.data.Filter = filter
}
